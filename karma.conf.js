const path = require('path');
const webpack = require('webpack');

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine-ajax', 'jasmine'],
    files: ['*/**/*.spec.ts'],
    preprocessors: {
      '*/**/*.spec.ts': ['webpack', 'sourcemap'],
    },
    mime: {
      'text/x-typescript': ['ts']
    },
    plugins: [
      'karma-chrome-launcher',
      'karma-jasmine',
      'karma-typescript',
      'karma-webpack',
      'karma-source-map-support',
      'karma-sourcemap-loader',
      'karma-coverage-istanbul-reporter',
      'karma-jasmine-ajax'
    ],
    webpack: {
      mode: 'development',
      resolve: {
        extensions: ['.ts', '.js', '.html'],
      },
      module: {
        rules: [
          {
            test: /.ts?$/,
            loader: 'babel-loader',
            include: [path.resolve(__dirname, 'src')],
            exclude: [/node_modules/]
          },
          {
            test: /\.template.html$/,
            loader: ['html-loader'],
            include: [path.resolve(__dirname, 'src')],
            exclude: [/node_modules/]
          }
        ]
      },
      stats: {
        colors: true,
        modules: true,
        reasons: true,
        errorDetails: true
      },
      plugins: [
        new webpack.SourceMapDevToolPlugin({
          filename: null,
          test: /\.(ts)($|\?)/i,
          exclude: [/node_modules/]
        })
      ],
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: true,
    concurrency: Infinity
  })
}