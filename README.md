#Dott: Assignment, Carlos Kloske


## Clone the repository

Get the project from Bitbucket:

```git clone git@bitbucket.org:c_kloske/assignment.git```


## Install

Go to `/assignment` and install npm dependencies:

```npm install```


## Clone a repository

Run the project:

```npm start```

A new tab should open your your browser automatically. If it doesn't happen, go to `http://localhost:8080/`. 

In Chrome, please. :-)

## Notes

### Things I missed
* "There are multiple sizes of images available in the API, choose the correct size based on a viewport size": Sorry...

### Things I sort of did...
* "Load images lazily as the carousel scrolls with a small buffer of a few images": it's not the best implementation I've ever seen, for sure...

### What could I have done better

* **Code organization:** I'd probably split functionalities in more services, leaving only the main logic in the componenets. Some of them, especially the carousel component, are very cluttered and ended up being quite confusing.
	* Probably it's a good idea to move the voice recognition code to a dedicated service
* **Write unit tests for everything**
* **Move CSS to a dedicated (SASS) space**
* **Make everything responsible:** I did just a very small part of it so far; it's not responsible.
* **Cover all cases:** the carousel doesn't work well with less than 5 results. 

### Notes

I wanted to have some animation in the button and loading icons. They were working until they weren't and I didn't wnat to spend more time investigating the issue.

I did not implement the logic to go left or right on the list. I was halfway there but I realized I was spending too much time already on the assignment.

Unit test of the components is missing. I couldn't decide on the best suite to use to test them. Ended up using Jasmine (I'm used to it), but it looks like it was a mistake.

It's possible that some dependencies in the package.json are not being used. I didn't have time to clean it up.

### Thank you

I appreciate the opportunity to make this assignment. I had a lot of fun (and some stress). but mostly it was a very nice way to experiment with new things (at least for me), like voice recognition, and even custom components, since I don't work with them in a day-to-day basis. I probably made some mistakes, I guess.