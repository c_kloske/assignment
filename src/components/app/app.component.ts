import tmp from './app.template.html';

const template = document.createElement('template');
template.innerHTML = tmp;

export class RootComponent extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}
