import { expect, fixture } from '@open-wc/testing';
import { RootComponent } from "./app.component";
import template from './app.template.html';

customElements.define('root-test', RootComponent);

describe('RootComponent', () => {
  it('renders the template as expected', async () => {
    const el = await fixture(`<root-test></root-test>`);
    expect(el).shadowDom.to.equal(template);
  });
});
