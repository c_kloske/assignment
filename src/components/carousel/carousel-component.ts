import { CollectionService } from '../../services/collection/collection-service';
import { Book, OpenLibraryService } from '../../services/open-library/open-library-service';
import { carouselConfig } from './carousel.config';
import tmp from './carousel.template.html';

const template = document.createElement('template');
template.innerHTML = tmp;

const collectionService = new CollectionService();
const openLibraryService = new OpenLibraryService();

export class CarouselComponent extends HTMLElement {
  collectionService = collectionService;
  openLibraryService: OpenLibraryService;
  isFetching: boolean;
  hasError: boolean;
  hasResults: boolean;
  books: Book[];
  hasInteraction: boolean;
  isScrolling: boolean;
  slideNumber: number;
  autoScroll: number;

  carousel: HTMLElement;
  slider: HTMLElement;
  arrowLeft: HTMLElement;
  arrowRight: HTMLElement;
  timer: HTMLElement;
  loading: HTMLElement;
  noResults: HTMLElement;
  collection: HTMLElement[];

  constructor() {
    super();

    this.hasInteraction = false;

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));

    this.openLibraryService = openLibraryService;

    document.addEventListener('search-books', (e: CustomEvent) => {
      this.getBooks(e.detail);
    });

    // this.arrowLeft.addEventListener('click', this.moveLeft.bind(this));
    // this.arrowRight.addEventListener('click', this.moveRight.bind(this));
  }

  connectedCallback() {
    this.isFetching = true;

    this.arrowLeft = this.shadowRoot.querySelector('.left');
    this.arrowRight = this.shadowRoot.querySelector('.right');
    this.carousel = this.shadowRoot.querySelector('.carousel');
    this.slider = this.shadowRoot.querySelector('.slider');
    this.timer = this.shadowRoot.querySelector('.last-search');
    this.loading = this.shadowRoot.querySelector('.loading');
    this.noResults = this.shadowRoot.querySelector('.no-result');

    this.carousel.setAttribute('style', `background-color: ${carouselConfig.useColor}`);

    this.carousel.addEventListener('click', this.toggleAnimation.bind(this));
  }

  disconnectedCallback() {
    window.clearInterval(this.autoScroll);
  }

  private reset() {
    this.slideNumber = 0;
    this.collection = [];
    this.isScrolling = true;
  }

  private getBooks(query: string) {
    this.timer.classList.remove('active');
    window.clearInterval(this.autoScroll);
    this.loading.classList.add('active');
    this.slider.classList.remove('active');
    this.noResults.classList.remove('active');

    this.openLibraryService.getBooks(query)
      .then((books) => {
        this.reset();
        this.hasError = false;
        this.hasResults = Boolean(books.length);
        this.isFetching = false;
        this.books = books;

        this.loading.classList.remove('active');

        // renders each book in an element and add it to the slider element
        const bookElements: HTMLElement[] = [];
        books.forEach((book: Book, i: number) => {

          // create an author string with the year if available
          const author = book.year ? `${book.author}, ${book.year}` : book.author;

          // create the item,  assign all the attributes and append to the slider
          const item = document.createElement('ck-carousel-item');
          item.setAttribute('book-id', String(i));
          item.setAttribute('book-title', book.title);
          item.setAttribute('book-author', author);
          if (book.hasImage) {
            item.setAttribute('book-image', book.image.image_hi);
          }

          // appends the item to the slider element
          bookElements.push(item);
        });

        if (this.hasResults) {
          // add autoScroll
          this.autoScroll = window.setInterval(() => {
            if (!this.hasInteraction && this.isScrolling) {
              this.isScrolling = true;
              this.moveRight();
            }
          }, 3000);

          // move things around
          this.timer.classList.add('active');
          this.slider.classList.add('active');
          this.collection = [...bookElements];
          this.updateSlider();
        } else {
          this.slider.classList.remove('active');
          this.noResults.classList.add('active');
        }

      })
      .catch((e) => {
        this.hasError = true;
        this.isFetching = false;
        this.noResults.classList.add('active');
        console.warn('Error: ', e);
      });
  }

  private updateSlider(): void {
    const sortedSlides = this.collectionService.getSlidesSorted(this.slideNumber, this.collection);
    const newSlider = this.collectionService.createNewSlider(sortedSlides);
    this.carousel.replaceChild(newSlider, this.slider);
    this.slider = this.shadowRoot.querySelector('.slider');
  }

  /**
   * Moves the slides by creating a new slider element
   *
   * @param direction -1 is left, 1 is right
   */
  private moveSlide(direction: 1 | -1): void {
    let newSlideNumber = this.slideNumber + direction;
    if (newSlideNumber < 0) {
      newSlideNumber = this.collection.length - 1;
    }
    if (newSlideNumber > this.collection.length - 1) {
      newSlideNumber = 0;
    }

    this.slideNumber = newSlideNumber;
  }

  private moveLeft(): void {
    const moveLeftCallback = () => {
      this.moveSlide(-1);
      this.updateSlider();
      this.slider.classList.remove('animate-left');
      this.slider.removeEventListener('transitionend', moveLeftCallback);
    };
    this.slider.addEventListener('webkitTransitionEnd', moveLeftCallback);
    this.slider.classList.add('animate-left');
  }

  private moveRight(): void {
    const moveRightCallback = () => {
      this.moveSlide(1);
      this.updateSlider();
      this.slider.classList.remove('animate-right');
      this.slider.removeEventListener('transitionend', moveRightCallback);
    };
    this.slider.addEventListener('webkitTransitionEnd', moveRightCallback);
    this.slider.classList.add('animate-right');
  }

  private toggleAnimation(): void {
    this.isScrolling = !this.isScrolling;
  }
}
