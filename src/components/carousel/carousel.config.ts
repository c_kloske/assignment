const availableColors = ['#9bf3db', '#fcc0d4', '#75d4ff', '#fdf476' ];

export const carouselConfig = {
  colors: ['#9bf3db', '#fcc0d4', '#75d4ff', '#fdf476' ],
  useColor: availableColors[2],
};
