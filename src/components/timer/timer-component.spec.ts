import { expect, fixture } from '@open-wc/testing';
import { TimerComponent } from './timer-component';
import template from './timer.template.html';

customElements.define('timer-test', TimerComponent);

describe('TimerComponent', () => {
  it('renders the template as expected', async () => {
    const el = await fixture(`<timer-test></carousel-test>`);
    expect(el).shadowDom.to.equal(template);
  });
});
