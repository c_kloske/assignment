import tmp from './timer.template.html';

import { TimerService } from '../../services/timer/timer-service';

const template = document.createElement('template');
template.innerHTML = tmp;

const timerService = new TimerService();

export class TimerComponent extends HTMLElement {
  private timer: number;
  private timerText: HTMLElement;

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  connectedCallback() {
    this.timerText = this.shadowRoot.querySelector('p');
    this.shadowRoot.appendChild(this.timerText);
    document.addEventListener('search-books', () => {
      this.startTimer();
    });
  }

  disconnectedCallback() {
    window.clearInterval(this.timer);
  }

  private startTimer(): void {
    timerService.setTime();
    this.timerText.innerText = timerService.getTime();
    function dealWithTimer() {
      this.timerText.innerText = timerService.getTime();
      return dealWithTimer;
    }
    this.timer = window.setInterval(
      dealWithTimer.bind(this)
      , 10000);
  }

}
