import { expect, fixture } from '@open-wc/testing';
import { CarouselItemComponent } from './carousel-item.component';
import template from './carousel-item.template.html';

customElements.define('carousel-item-test', CarouselItemComponent);

describe('CarouselItemComponent', () => {
  it('renders the template as expected', async () => {
    const el = await fixture(`<carousel-item-test></carousel-item-test>`);
    expect(el).shadowDom.to.equal(template);
  });
});
