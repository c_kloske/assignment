import tmp from './carousel-item.template.html';

const template = document.createElement('template');
template.innerHTML = tmp;

export class CarouselItemComponent extends HTMLElement {
  containerArea: HTMLElement;
  imageArea: HTMLElement;
  isImageLoaded: boolean;
  isImageAttached: boolean;
  descriptionArea: HTMLElement;
  titleArea: HTMLElement;
  authorArea: HTMLElement;

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    this.isImageLoaded = false;
    this.isImageAttached = false;
  }

  static get observedAttributes() {
    return ['book-active'];
  }

  connectedCallback() {
    this.containerArea = this.shadowRoot.querySelector('.container');
    this.imageArea = this.containerArea.querySelector('.image');
    this.descriptionArea = this.containerArea.querySelector('.description');
    this.titleArea = this.descriptionArea.querySelector('h1');
    this.authorArea = this.descriptionArea.querySelector('p');
    this.render();
  }

  attributeChangedCallback(name, oldValue, newValue) {
    // attributeChangedCallback is being called before connectedCallback
    if (this.containerArea) {
      this.render(name);
    }
  }

  private render(name?: string): void {
    const attrs = this.getAttributeNames();
    [...attrs].forEach((attr) => {
      switch (attr) {
        case 'book-image':
          const img = new Image();
          if (!this.isImageAttached) {
            this.imageArea.appendChild(img);
            this.isImageAttached = true;
          }
          img.addEventListener('load', () => {
            img.classList.add('loaded');
            this.isImageLoaded = true;
          }, false);
          img.addEventListener('error', (e) => {
            this.imageArea.classList.add('no-image');
            e.preventDefault();
          }, false);
          if (!this.isImageLoaded) {
            img.src = this.getAttribute(attr);
          }
          break;
        case 'book-title':
          this.titleArea.textContent = this.getAttribute(attr);
          break;
        case 'book-author':
          this.authorArea.textContent = this.getAttribute(attr);
          break;

      }
    });
  }
}
