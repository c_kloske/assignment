import tmp from './search.template.html';

const template = document.createElement('template');

template.innerHTML = tmp;

export class SearchComponent extends HTMLElement {
  input: HTMLInputElement;
  searchButton: HTMLButtonElement;
  speechButton: HTMLButtonElement;
  eraseButton: HTMLElement;
  recognition: WebkitSpeechRecognition;
  transcript: string;
  isRecording: boolean;

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    this.input = this.shadowRoot.querySelector('input');
    this.input.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        const searchEvent = new CustomEvent('search-books',
        { bubbles: true, composed: true, detail: this.input.value },
        );
        this.dispatchEvent(searchEvent);
      }
    });
    this.searchButton = this.shadowRoot.querySelector('.search');
    this.speechButton = this.shadowRoot.querySelector('.speech');
    this.eraseButton = this.shadowRoot.querySelector('.input');
    this.searchButton.addEventListener('click', this.dispatchSearch.bind(this));
    this.speechButton.addEventListener('click', this.startSpeechRecognition.bind(this));
    this.eraseButton.addEventListener('click', () => this.setSearchText.call(this, ''));
  }

  connectedCallback() {
    this.searchButton.removeEventListener('click', this.dispatchSearch);
  }

  private dispatchSearch(): void {
    const searchEvent = new CustomEvent('search-books', { bubbles: true, composed: true, detail: this.input.value });
    this.dispatchEvent(searchEvent);
  }

  private setSearchText(text = ''): void {
    this.input.value = text;
  }

  private startSpeechRecognition(): void {
    if (!this.isRecording) {
      this.recognition = new webkitSpeechRecognition();
      this.recognition.onresult = (event) => {
        let interimResults = '';
        let results = '';
        for (let i = event.resultIndex; i < event.results.length; ++i) {
          if (event.results[i].isFinal) {
            results += event.results[i][0].transcript;
            this.setSearchText(results);
            this.stopSpeechRecognition();
          } else {
            interimResults += event.results[i][0].transcript;
            this.setSearchText(interimResults);
          }
        }
      };
      this.speechButton.classList.add('recording');
      this.isRecording = true;
      this.setSearchText('');
      this.recognition.interimResults = true;
      this.recognition.start();
    } else {
      this.stopSpeechRecognition();
    }
  }

  private stopSpeechRecognition(): void {
    this.speechButton.classList.remove('recording');
    this.isRecording = false;
    this.recognition.stop();
    this.dispatchSearch();
  }
}
