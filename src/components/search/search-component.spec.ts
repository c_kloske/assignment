import { expect, fixture } from '@open-wc/testing';
import { SearchComponent } from './search-component';
import template from './search.template.html';

customElements.define('search-test', SearchComponent);

describe('SearchComponent', () => {
  it('renders the template as expected', async () => {
    const el = await fixture(`<search-test></search-test>`);
    expect(el).shadowDom.to.equal(template);
  });
});
