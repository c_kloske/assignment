import { RootComponent } from './components/app/app.component';
import { CarouselItemComponent } from './components/carousel-item/carousel-item.component';
import { CarouselComponent } from './components/carousel/carousel-component';
import { SearchComponent } from './components/search/search-component';
import { TimerComponent } from './components/timer/timer-component';

customElements.define('root-component', RootComponent);
customElements.define('ck-carousel', CarouselComponent);
customElements.define('ck-carousel-item', CarouselItemComponent);
customElements.define('ck-search', SearchComponent);
customElements.define('ck-timer', TimerComponent);
