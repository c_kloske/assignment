import { openLibraryConfig } from './open-library.config';

export interface Book {
  title: string;
  author: string;
  year: string;
  id: string;
  hasImage: boolean;
  image?: {
    image_lo?: string;
    image_hi?: string;
  };
}

export class OpenLibraryService {
  getBooks(q: string): Promise<any[]> {
    return this.get(
      `${openLibraryConfig.getBooks.url}?q=${this.formatQuery(q)}&limit=${openLibraryConfig.getBooks.limit}`,
    )
      .then((response) => {
        const result: any = JSON.parse(response) || {};
        const documents: any[] = result.docs as any[] || [];
        return documents.map((b): Book => {
          const title = b.title || b.title_suggest;
          const author: string = b.author_name ? b.author_name.toString() : undefined;
          const editionKey = b.edition_key ? b.edition_key[0] : null;
          const id: string = b.cover_edition_key || editionKey;
          return {
            title,
            author,
            year: b.first_publish_year || null,
            id,
            hasImage: Boolean(id),
            image: {
              image_lo: id ? this.getCoverUrl(id, 'M') : null,
              image_hi: id ? this.getCoverUrl(id, 'L') : null,
            },
          };
        });
      });
  }

  private getCoverUrl(id: string, size: string): string {
    const coverUrl = `${openLibraryConfig.getCover.url}${id}-${size}.jpg?default=false`;
    return coverUrl;
  }

  private formatQuery(q: string): string {
    return q.replace(/\s/g, '+');
  }

  private get(url: string): Promise<any> {
    return new Promise<any>(
      (resolve, reject) => {
        const request = new XMLHttpRequest();
        request.onload = function() {
          if (this.status === 200) {
            resolve(this.response);
          } else {
            reject(new Error(this.statusText));
          }
        };
        request.onerror = function() {
          reject(new Error('XMLHttpRequest Error: ' + this.statusText));
        };
        request.open('GET', url, true);
        request.send();
      });
  }

}
