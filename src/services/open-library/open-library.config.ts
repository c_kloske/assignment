export const openLibraryConfig = {
  getBooks: {
    limit: 50,
    url: 'http://openlibrary.org/search.json',
  },
  getCover: {
    limit: 10,
    url: 'http://covers.openlibrary.org/b/olid/',
  },
};
