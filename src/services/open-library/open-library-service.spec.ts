import { OpenLibraryService } from './open-library-service';

const mockData = {
  numFound: 9,
  start: 0,
  docs: [
    {
      title_suggest: 'The Tales of Beedle the Bard',
      edition_key: [
        'OL25565942M',
        'OL23720575M',
        'OL25425988M',
        'OL23102899M',
        'OL23176734M',
        'OL22861514M',
        'OL25552440M',
        'OL26410157M',
        'OL26321762M',
      ],
      cover_i: 7890711,
      id_librarything: [
        '5927153',
      ],
      has_fulltext: true,
      text: [
        'OL25565942M',
        'OL23720575M',
        'OL25425988M',
        'OL23102899M',
        'OL23176734M',
        'OL22861514M',
        'OL25552440M',
        'OL26410157M',
        'OL26321762M',
        '978-83-8008-297-7',
        '8000022842',
        '8532516017',
        '9788532516015',
        '0545128285',
        '9788000022840',
        '2070625192',
        '2070623440',
        '9782070625192',
        '978-2070625192',
        '978-2070623440',
        '5353040171',
        '0747599874',
        '9780747599876',
        '9780545128285',
        '9785353040170',
        '8380082974',
        'J. K. Rowling',
        'Children\'s High Level Group',
        'Pavel Medek (translation)',
        '845018888',
        '470584776',
        '316678756',
        'talesofbeedlebar00rowl',
        'contosdebeedleob00rowl',
        'talesbeedlebards00rowl_187',
        'isbn_9785353040170',
        'OL23919A',
        'In library',
        'Magicians',
        'Magic',
        'juvenile fiction',
        'Hogwarts School of Witchcraft and Wizardry (Imaginary place)',
        'Wizards',
        'Hogwarts School of Witchcraft and Wizardry (Imaginary organization)',
        'Warlocks',
        'Fiction',
        'Children\s stories, English',
        'Fantasy',
        'Accessible book',
        'Fairy tales',
        'New York Times bestseller',
        'Witches',
        'Protected DAISY',
        'Short stories',
        'Juvenile fiction',
        'nyt:series_books=2006-09-16',
        'The Tales of Beedle the Bard',
        '/works/OL13716951W',
        'by J.K. Rowling.',
        'Translated from the Ancient Runes by Hermione Granger ; Commentary by Albus Dumbledore ; Introduction, Notes, and Illustrations by J.K. Rowling.',
        'by J.K. Rowling',
        'traduzidos das runas originais por Hermione Granger ; de J.K. Rowling',
        'translated from the ancient runes by Hermione Granger ; commentary by Albus Dumbledore ; introduction, notes, and illustrations by J.K. Rowling.',
        'Albatros',
        'ROSM',
        'In association with Arthur A. Levine Books',
        'Bloomsbury',
        'Media Rodzina',
        'Gallimard',
        'Arthur A. Levine Books',
        'Children\'s High Level Group',
        'Rocco',
        'Childrens High Level Group',
        'Tales of beedle the bard',
        'The tales of Beedle the Bard',
        'Os contos de beedle, o bardo',
        'Tales of Beedle the Bard :',
        'Ba\u015bnie barda Beedle\'a',
        'Tales of Beedle the Bard',
        'Skazki Barda Bidlia',
        'Les Contes de Beedle le Barde',
        'Bajky barda Beedleho',
        '2008934360',
        '2009378135',
        '2008026889',
        'J K Rowling',
        'J. K. Rowling',
        'J.K. Rowling',
        'Jim Kay (illustrator) J.K. Rowling',
        'Joan K. Rowling',
        'Joanne Rowling',
        'JK Rowlings',
        'Rowling J.K',
        'J.K Rowling',
        'J.K. Rowling (author)',
        'j.k. rowling',
        'ROWLING, JOANNE K.',
        'Joanne \'Jo\' Rowling',
        'Joanne Murray, OBE',
        'ROWLING J.K.',
        'J.K.Rowling',
        'Jo Murray',
        'J. Rowling',
        'Rowling, J.K.',
        'Joanne K. Rowling',
        'There was once a kindly old wizard who used his magic generously and wisely for the benefit of his neighbors.',
      ],
      author_name: [
        'J. K. Rowling',
      ],
      contributor: [
        'Children\'s High Level Group',
        'Pavel Medek (translation)',
      ],
      ia_loaded_id: [
        'talesofbeedlebar00rowl',
      ],
      seed: [
        '/books/OL25565942M',
        '/books/OL23720575M',
        '/books/OL25425988M',
        '/books/OL23102899M',
        '/books/OL23176734M',
        '/books/OL22861514M',
        '/books/OL25552440M',
        '/books/OL26410157M',
        '/books/OL26321762M',
        '/works/OL13716951W',
        '/subjects/juvenile_fiction',
        '/subjects/in_library',
        '/subjects/magicians',
        '/subjects/magic',
        '/subjects/hogwarts_school_of_witchcraft_and_wizardry_(imaginary_place)',
        '/subjects/warlocks',
        '/subjects/fairy_tales',
        '/subjects/children\'s_stories_english',
        '/subjects/wizards',
        '/subjects/new_york_times_bestseller',
        '/subjects/protected_daisy',
        '/subjects/fiction',
        '/subjects/short_stories',
        '/subjects/nyt:series_books=2006-09-16',
        '/subjects/juvenile_fiction',
        '/subjects/witches',
        '/subjects/hogwarts_school_of_witchcraft_and_wizardry_(imaginary_organization)',
        '/authors/OL23919A',
      ],
      oclc: [
        '845018888',
        '470584776',
        '316678756',
      ],
      ia: [
        'talesofbeedlebar00rowl',
        'contosdebeedleob00rowl',
        'talesbeedlebards00rowl_187',
        'isbn_9785353040170',
      ],
      isbn: [
        '978-83-8008-297-7',
        '8000022842',
        '8532516017',
        '9788532516015',
        '0545128285',
        '9788000022840',
        '2070625192',
        '2070623440',
        '9782070625192',
        '978-2070625192',
        '978-2070623440',
        '5353040171',
        '0747599874',
        '9780747599876',
        '9780545128285',
        '9785353040170',
        '8380082974',
      ],
      author_key: [
        'OL23919A',
      ],
      subject: [
        'In library',
        'Magicians',
        'Magic',
        'juvenile fiction',
        'Hogwarts School of Witchcraft and Wizardry (Imaginary place)',
        'Wizards',
        'Hogwarts School of Witchcraft and Wizardry (Imaginary organization)',
        'Warlocks',
        'Fiction',
        'Children\'s stories, English',
        'Fantasy',
        'Accessible book',
        'Fairy tales',
        'New York Times bestseller',
        'Witches',
        'Protected DAISY',
        'Short stories',
        'Juvenile fiction',
        'nyt:series_books=2006-09-16',
      ],
      title: 'The Tales of Beedle the Bard',
      lending_identifier_s: 'talesofbeedlebar00rowl',
      ia_collection_s: 'printdisabled;americana;internetarchivebooks;delawarecountydistrictlibrary;librarygenesis;china;inlibrary',
      publish_date: [
        '2008',
        '2009',
        '2017',
      ],
      type: 'work',
      ebook_count_i: 4,
      publish_place: [
        'Prague',
        'Rio de Janeiro',
        'Moscow, Russia',
        'Pozna\u0144, Poland',
        'New York',
        'London',
      ],
      ia_box_id: [
        'IA129615',
      ],
      edition_count: 9,
      first_publish_year: 2008,
      key: '/works/OL13716951W',
      id_goodreads: [
        '4020390',
        '3950967',
      ],
      public_scan_b: false,
      publisher: [
        'Albatros',
        'ROSM',
        'In association with Arthur A. Levine Books',
        'Bloomsbury',
        'Media Rodzina',
        'Gallimard',
        'Arthur A. Levine Books',
        'Children\'s High Level Group',
        'Rocco',
        'Childrens High Level Group',
      ],
      language: [
        'rus',
        'pol',
        'por',
        'fre',
        'eng',
      ],
      lccn: [
        '2008934360',
        '2009378135',
        '2008026889',
      ],
      last_modified_i: 1557798629,
      lending_edition_s: 'OL22861514M',
      author_alternative_name: [
        'J K Rowling',
        'J. K. Rowling',
        'J.K. Rowling',
        'Jim Kay (illustrator) J.K. Rowling',
        'Joan K. Rowling',
        'Joanne Rowling',
        'JK Rowlings',
        'Rowling J.K',
        'J.K Rowling',
        'J.K. Rowling (author)',
        'j.k. rowling',
        'ROWLING, JOANNE K.',
        'Joanne \'Jo\' Rowling',
        'Joanne Murray, OBE',
        'ROWLING J.K.',
        'J.K.Rowling',
        'Jo Murray',
        'J. Rowling',
        'Rowling, J.K.',
        'Joanne K. Rowling',
      ],
      cover_edition_key: 'OL22861514M',
      first_sentence: [
        'There was once a kindly old wizard who used his magic generously and wisely for the benefit of his neighbors.',
      ],
      publish_year: [
        2008,
        2009,
        2017,
      ],
      printdisabled_s: 'OL22861514M;OL23102899M;OL25552440M;OL25565942M',
    }],
};

const openLibraryService = new OpenLibraryService();

describe('OpenLibraryService', () => {

  beforeEach(() => {
    jasmine.Ajax.install();
  });

  afterEach(() => {
    jasmine.Ajax.uninstall();
  });

  describe('getBooks', () => {
    it('makes the expected call', async () => {

      openLibraryService.getBooks('the tales of beedle the bard');
      // TODO: test the formatted response

      const request = jasmine.Ajax.requests.mostRecent();

      request.respondWith(mockData as unknown as XMLHttpRequest);

      expect(jasmine.Ajax.requests.mostRecent().url)
        .toBe('http://openlibrary.org/search.json?q=the+tales+of+beedle+the+bard&limit=50');
      expect(request.method).toBe('GET');
    });
  });
});
