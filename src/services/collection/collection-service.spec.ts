import { CollectionService } from './collection-service';

const collectionService = new CollectionService();

describe('CollectionService', () => {

  beforeEach(() => {
    jasmine.Ajax.install();
  });

  afterEach(() => {
    jasmine.Ajax.uninstall();
  });

  describe('getSlidesSorted', () => {
    it('returns the expected result', async () => {
      const testArray = [0, 1, 2, 3, 4, 5] as unknown as HTMLElement[];
      const result = collectionService.getSlidesSorted(0, testArray);
      expect(result).toEqual([4, 5, 0, 1, 2]);
    });
    it('works also with smaller arrays', async () => {
      const testArray = [0, 1, 2] as unknown as HTMLElement[];
      const result = collectionService.getSlidesSorted(0, testArray);
      expect(result).toEqual([1, 2, 0, 1, 2]);
    });
  });
  describe('createNewSlider', () => {
    it('returns a new div element', async () => {
      const element = document.createElement('div');
      const testArray = [element, element, element, element, element];
      const result = collectionService.createNewSlider(testArray);
      expect(result.nodeName).toBe('DIV');
    });
  });
});
