export class CollectionService {

  /**
   * Creates a new array where the pivot index is the center of 5 items
   * @param pivot the center index of the new array
   * @param originalArray the array containing all slides from the search results
   */
  getSlidesSorted(pivot: number, originalArray: HTMLElement[]) {
    const right = this.takeRight(2, pivot, originalArray);
    const left = this.takeLeft(2, pivot, originalArray);
    return [...left, originalArray[pivot], ...right];
  }

  /**
   * Creates a new slider from an array of slides
   * @param slidesArray an array of carousel items html elements
   */
  createNewSlider(slidesArray: HTMLElement[]): HTMLElement {
    const newSlider = document.createElement('div');
    newSlider.classList.add('slider', 'active');
    slidesArray.forEach((elem) => newSlider.appendChild(elem));
    return newSlider;
  }
  /**
   * Takes two array items from the right of the provided pivot index
   * @param total how many items to take
   * @param pivot the center of the new array, relatively to the original array
   * @param originalArray the original array to be used
   */
  private takeRight(total: number, pivot: number, originalArray: any[]) {
    let nextIndex = pivot;
    const result = [];

    function nextRight(index: number) {
      if (index >= originalArray.length - 1) {
        return 0;
      } else {
        return index + 1;
      }
    }

    for (let n = 0; n < total; n++) {
      nextIndex = nextRight(nextIndex);
      result.push(originalArray[nextIndex]);
    }
    return result;
  }
  /**
   * Takes two array items from the left of the provided pivot index
   * @param total how many items to take
   * @param pivot the center of the new array, relatively to the original array
   * @param originalArray the original array to be used
   */
  private takeLeft(total: number, pivot: number, originalArray: any[]) {
    let nextIndex = pivot;
    const result = [];

    function nextLeft(index: number) {
      if (index === 0) {
        return originalArray.length - 1;
      } else {
        return index - 1;
      }
    }

    for (let n = 0; n < total; n++) {
      nextIndex = nextLeft(nextIndex);
      result.push(originalArray[nextIndex]);
    }
    return result.reverse();
  }
}
