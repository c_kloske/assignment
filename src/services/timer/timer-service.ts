export class TimerService {
  private previousTime: Date;

  constructor() {
    this.previousTime = new Date();
  }

  getTime(): string {
    const lang = navigator.language;
    const currentTime = new Date();
    const timeDifferenceSeconds = Math.floor((currentTime.getTime() - this.previousTime.getTime()) / 1000);
    const localeTime = new Intl.RelativeTimeFormat(lang, { style: 'long', numeric: 'auto' });
    if (timeDifferenceSeconds > 3599) {
      return localeTime.format(-Math.floor(timeDifferenceSeconds / 3600), 'hour');
    }
    if (timeDifferenceSeconds > 59) {
      return localeTime.format(-Math.floor(timeDifferenceSeconds / 60), 'minute');
    }
    return localeTime.format(-(timeDifferenceSeconds), 'second');
  }

  setTime(): void {
    this.previousTime = new Date();
  }
}
